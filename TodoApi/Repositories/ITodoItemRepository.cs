﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TodoApi.Models;

namespace TodoApi.Repositories
{
    public interface ITodoItemRepository
    {
        Task AddTodoItem(string authorName, TodoItem todoItem);

        Task RemoveTodoItemById(string authorName, Guid id);

        Task<IList<TodoItem>> GetAllTodoItems(string authorName);
    }
}
