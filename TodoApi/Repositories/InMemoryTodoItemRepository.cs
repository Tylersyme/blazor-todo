﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TodoApi.Extensions;
using TodoApi.Models;

namespace TodoApi.Repositories
{
    public class InMemoryTodoItemRepository : ITodoItemRepository
    {
        private IDictionary<string, IList<TodoItem>> _todoItems;

        public InMemoryTodoItemRepository()
        {
            _todoItems = new Dictionary<string, IList<TodoItem>>();
        }

        public Task AddTodoItem(string authorName, TodoItem todoItem)
        {
            if (!this.AuthorExists(authorName))
                _todoItems.Add(authorName, new List<TodoItem>());

            _todoItems[authorName].Add(todoItem);

            return Task.CompletedTask;
        }

        public Task RemoveTodoItemById(string authorName, Guid id)
        {
            if (!this.AuthorExists(authorName))
                throw new Exception();

            _todoItems[authorName].RemoveAll((ti) => ti.Id == id);

            return Task.CompletedTask;
        }

        public Task<IList<TodoItem>> GetAllTodoItems(string authorName)
        {
            if (!this.AuthorExists(authorName))
                throw new Exception();

            return Task.FromResult(_todoItems[authorName]);
        }

        private bool AuthorExists(string authorName) =>
            _todoItems.ContainsKey(authorName);
    }
}
