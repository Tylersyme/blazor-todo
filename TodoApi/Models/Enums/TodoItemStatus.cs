﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TodoApi.Models.Enums
{
    public enum TodoItemStatus
    {
        NotStarted,
        InProgress,
        Completed,
    }
}
