﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TodoApi.Extensions
{
    public static class IListExtensions
    {
        public static void RemoveAll<TElement>(this IList<TElement> list, Predicate<TElement> match)
        {
            for (var idx = list.Count - 1; idx >= 0; idx--)
            {
                if (!match(list[idx]))
                    continue;

                list.RemoveAt(idx);
            }
        }
    }
}
