﻿using Blazored.LocalStorage;
using BlazorTodo.Extensions;
using BlazorTodo.Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorTodo.Services
{
    public class TodoItemLocalStorageRepository : ITodoItemRepository
    {
        private const string TODOS_KEY = "todos";

        private readonly ILocalStorageService _localStorageService;

        public TodoItemLocalStorageRepository(ILocalStorageService localStorageService)
        {
            _localStorageService = localStorageService;
        }

        public async Task AddTodoItemAsync(TodoItem todoItem)
        {
            var todoItems = await this.GetTodoItemsAsync();

            todoItems.Add(todoItem);

            await _localStorageService.SetItemAsync(TODOS_KEY, todoItems);
        }

        public async Task RemoveTodoItemByIdAsync(Guid id)
        {
            var todoItems = await this.GetTodoItemsAsync();

            todoItems.RemoveAll(ti => ti.Id == id);

            await _localStorageService.SetItemAsync(TODOS_KEY, todoItems);
        }
        public async Task<IList<TodoItem>> GetTodoItemsAsync()
        {
            var todos = new List<TodoItem>();

            if (await _localStorageService.ContainKeyAsync(TODOS_KEY))
                todos = await _localStorageService.GetItemAsync<List<TodoItem>>(TODOS_KEY);

            return todos;
        }
    }
}
