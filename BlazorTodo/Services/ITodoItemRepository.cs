﻿using BlazorTodo.Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorTodo.Services
{
    public interface ITodoItemRepository
    {
        Task<IList<TodoItem>> GetTodoItemsAsync();

        Task AddTodoItemAsync(TodoItem todoItem);

        Task RemoveTodoItemByIdAsync(Guid id);
    }
}
